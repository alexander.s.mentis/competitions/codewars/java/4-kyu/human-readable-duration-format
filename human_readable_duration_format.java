import java.util.ArrayList;

public class TimeFormatter {
    
    public static String formatDuration(int seconds) {
      // your code goes here
      if (seconds == 0) return "now";
      
      final int secPerYr  = 31_536_000;
      final int secPerDay = 86_400;
      final int secPerHr  = 3_600;
      final int secPerMin = 60;
      
      ArrayList<String> stringParts = new ArrayList<>();
      
      int years = seconds / secPerYr;
      if (years != 0) {
        stringParts.add(years + (years > 1 ? " years" : " year"));
      }
       
      int days = seconds % secPerYr / secPerDay;
      if (days != 0) {
        stringParts.add(days + (days > 1 ? " days" : " day"));
      }
      
      int hours = seconds % secPerYr % secPerDay / secPerHr;
      if (hours != 0) {
        stringParts.add(hours + (hours > 1 ? " hours" : " hour"));
      }
      
      int mins = seconds % secPerYr % secPerDay % secPerHr / secPerMin;
      if (mins != 0) {
        stringParts.add(mins + (mins > 1 ? " minutes" : " minute"));
      }
      
      int secs = seconds % secPerYr % secPerDay % secPerHr % secPerMin;
      if (secs != 0) {
        stringParts.add(secs + (secs > 1 ? " seconds" : " second"));
      }
      
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < stringParts.size(); i++) {
        sb.append(stringParts.get(i));
        if (i != stringParts.size() - 1) {
          if (i == stringParts.size() - 2) {
            sb.append(" and ");
          } else {
            sb.append(", ");
          }
        }
      }
      
      return sb.toString();
    }
}